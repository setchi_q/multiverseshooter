﻿using UnityEngine;
using System.Collections;

public class Audio : MonoBehaviour {

	AudioClip clip;
	AudioSource source;

	//public GameObject audio1;
	private bool flag;

	private int count = 0;


	private static Audio instance = null;
	public static Audio Instance{
		get{return instance;}
	}

	void Awake(){
		if(instance != null && instance != this){
			Destroy(this.gameObject);
			return;
		}else{
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	}

	void Update () {
		if(Application.loadedLevelName=="Title"){
			if(count == 0){
				Destroy(this.gameObject);
			}else if(count == 1){
				audio.Play();
				count = 0;
			}
		}
		if(Application.loadedLevelName=="StageSelect"){
			count = 1;
		}
		if(Application.loadedLevelName=="CoreAction"){
			Destroy(this.gameObject);
		}
	}
}
