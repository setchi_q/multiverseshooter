﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

	SpriteRenderer MainSpriteRenaderer;

	public int attribute;
	public GameObject remainingBullet;

	public Sprite[] monsterSprites;
	
	private GameController gameController;
	private SpriteRenderer spriteRenderer;
	private GUIText remainingBulletGUIText;

	private CircularGauge circularGauge; 
	
	private int bulletNum;
	private int remainingBulletNum;
	
	private int bulletShootCount = 0;
	private int skillCondition;
	
	private static int coolTime = 5;
	private float coolStartTime = -coolTime;
	
	private int flashingType = 0;
	
	private GameObject party;
	private DataOutPut dataOutPut;
	private PowerData powerData;
	public int position;//left = 0 center = 1 right = 2
	private int monsterNo;

	void Awake() {
		
		gameController = GameObject.Find ("GameController").GetComponent<GameController> ();
		spriteRenderer = GetComponent<SpriteRenderer>();
		remainingBulletGUIText = remainingBullet.GetComponent<GUIText>();
		circularGauge = gameObject.GetComponentInChildren<CircularGauge> ();
		
		powerData = GetComponent<PowerData>();
		party = GameObject.Find ("Party");
		if (party){
			dataOutPut = party.GetComponent<DataOutPut>();
			switch (position){
			case 0:
				monsterNo = dataOutPut.OutputLeft();
				break;
			case 1:
				monsterNo = dataOutPut.OutputCenter();
				break;
			case 2:
				monsterNo = dataOutPut.OutputRight();
				break;
			default:
				monsterNo = dataOutPut.OutputLeft();
				break;
			}
			int remainingNum = powerData.BulletRemaining(monsterNo);
			bulletNum = remainingNum;
			skillCondition = bulletNum * 3;
			SetRemainingBullet(bulletNum);
			SetRemainingNumText(bulletNum);
			attribute = powerData.Atribute(monsterNo);
			ChangeFriendSprite(dataOutPut.OutputFriend());
		}
	}
	void Start() {
		MainSpriteRenaderer = gameObject.GetComponent<SpriteRenderer>();
		ChangeButtonSprite();
	}
	
	void Update() {
		UpdateBulletCoolTime ();
	}
	
	void OnMouseDown() {
		gameController.ChangeCurrentAttribute (attribute);
	}

	void ChangeButtonSprite(){
		int index = monsterNo - 1;
		if (index < 0 || monsterSprites.Length - 1 < index)
						return;
		MainSpriteRenaderer.sprite = monsterSprites [index];
	}

	void ChangeFriendSprite(int friendNo) {
		int index = friendNo - 1;
		if (index < 0 || monsterSprites.Length - 1 < index)
			return;
		SpriteRenderer friendSpriteRenderer = GameObject.Find ("friend").GetComponent<SpriteRenderer>();
		friendSpriteRenderer.sprite = monsterSprites [index];
	}

	public void ButtonFlashing() {
		Hashtable iTweenFlashingParams = new Hashtable ();
		iTweenFlashingParams.Add ("from", new Vector2(1, 0));
		iTweenFlashingParams.Add ("to", new Vector2(0, 0));
		iTweenFlashingParams.Add ("time", 0.7f);
		iTweenFlashingParams.Add ("onupdate", "ChangeOpacity");
		iTweenFlashingParams.Add ("oncomplete", "ButtonFlashing");
		iTweenFlashingParams.Add ("oncompletetarget", gameObject);
		iTweenFlashingParams.Add ("onupdatetarget", gameObject);
		
		iTween.ValueTo (gameObject, iTweenFlashingParams);
	}
	
	void ChangeOpacity(Vector2 value) {
		Color color = spriteRenderer.color;
		color.a = value.x;
		if (attribute == 2) {
			color.a = Mathf.Abs(value.x - 0.5f) * 2;
		}
		spriteRenderer.color = color;
	}
	
	void RemainingBulletAnimationOnUpdate(Vector2 value) {
		remainingBulletGUIText.fontSize = Mathf.RoundToInt(value.x);
		remainingBulletGUIText.pixelOffset = new Vector2 (-(value.x - 60), (value.x - 60)) / 2;
		
		Color color = remainingBulletGUIText.color;
		color.a = 1 - (value.x - 60) / 240;
		remainingBulletGUIText.color = color;
	}
	
	void SetRemainingBullet(int remainingNum) {
		this.remainingBulletNum = remainingNum;
	}
	
	public void OneConsumesBullet() {
		bulletShootCount++;
		
		SetRemainingBullet (remainingBulletNum - 1);
		SetRemainingNumText (remainingBulletNum);
		if (this.remainingBulletNum == 0) {
			CoolTimeStart();
		}
		
		if (bulletShootCount > skillCondition) {
			// スキル発射可能状態
			bulletShootCount = 0;
			// ButtonFlashing ();
		}
	}

	void CoolTimeStart() {
		// クールダウン開始
		coolStartTime = Time.time;
		SetRemainingBullet(bulletNum);
		spriteRenderer.color = new Color(0.5f, 0.5f, 0.5f);
	}

	void CoolTimeFinish() {
		// クールタイムリセット
		coolStartTime = -5;
		SetRemainingNumText(bulletNum);
		spriteRenderer.color = new Color(1, 1, 1);
	}
	
	public void SetRemainingNumText(int num) {
		// if (remainingBulletGUIText.text.ToString() == num.ToString()) return;
		iTween.ValueTo(gameObject, iTween.Hash(
			"from", new Vector2(300, 300),
			"to", new Vector2(60, 60),
			"time", 0.6f,
			"onUpdate", "RemainingBulletAnimationOnUpdate",
			"onUpdateTarget", gameObject,
			"easeType", iTween.EaseType.easeOutExpo
		));  
		remainingBulletGUIText.text = num.ToString();
	}
	
	void UpdateBulletCoolTime() {
		if (coolStartTime == -5) {
			return;
		}
		float elapsedTime = Time.time - coolStartTime;
		UpdateCoolTimeUI(elapsedTime / coolTime);
		if (elapsedTime > coolTime) {
			CoolTimeFinish();
		}
	}
	
	void UpdateCoolTimeUI(float value) {
		circularGauge.SetValue(1 - value);
	}
	
	public bool IsCoolTimeDuring() {
		return Time.time - coolStartTime < coolTime;
	}

	public int GetAttribute() {
		return attribute;
	}
}
