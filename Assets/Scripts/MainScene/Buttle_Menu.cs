using UnityEngine;
using System.Collections;

public class Buttle_Menu : MonoBehaviour {

	public GameObject menu;
	private int count = 0;
	private bool flag = false;
	private bool flag2 = false; 
	private float timer;
	public GameObject touchAudio;
	private TouchAudio touch;
	
	void Start(){
		touch = touchAudio.GetComponent<TouchAudio>();
		if(Application.loadedLevelName=="StageSelect"){
			flag = true;
		}else{
			flag = false;
		}
	}

	void Update() {
		if(flag2){
			timer += Time.deltaTime;
			if(timer >0.3f){
				//Debug.Log("ok");
				Changescene();
			}
		}
	}
	void Changescene(){
		Application.LoadLevel("StageSelect");
	}
	void OnMouseDown(){
		touch.touch();
		if(count == 0){
			iTween.MoveTo(menu, iTween.Hash("x",-2.3, "islocal", true));
			count += 1;
		}
		if(!flag){
			timer = 0;
			flag2 = true;
		}
	}
	void OnMouseEnter(){
		if(count == 0){
			iTween.MoveTo(menu, iTween.Hash("x",-2.3, "islocal", true));
			count += 1;
		}
	}
	void OnMouseExit(){
		if(count == 1){
			iTween.MoveTo(menu, iTween.Hash("x", -3, "islocal", true));
			count -= 1;
		}
	}
	void OnMouseUp(){
		if(count == 1){
			iTween.MoveTo(menu, iTween.Hash("x", -3, "islocal", true));
			count -= 1;
		}
	}
}