﻿using UnityEngine;
using System.Collections;

public class PartyLeft : MonoBehaviour {

	SpriteRenderer MainSpriteRenaderer;

	//public GameObject parent;

	//Monster.No
	public Sprite no1;
	public Sprite no2;
	public Sprite no3;
	public Sprite no4;
	public Sprite no5;
	public Sprite no6;

	private int setNo;
	private int power;
	private int hitPoint;

	private int falseAttribute;
	private int attribute;
	private int attribute1;
	private int attribute2;

	private PartyRight right_att;
	private PartyCenter center_att;
	private SelectLeft no;

	private DataOutPut data;
	private PowerData data2;
	public GameObject right;
	public GameObject center;
	
	// Use this for initialization
	void Start () {

		setNo = 1;
		attribute = 0;

		MainSpriteRenaderer = gameObject.GetComponent<SpriteRenderer>();
		MainSpriteRenaderer.sprite = no1;
	 	
		data = GetComponentInParent<DataOutPut>();
		data2 = GetComponent<PowerData>();

		right_att = right.GetComponent<PartyRight>();

		center_att = center.GetComponent<PartyCenter>();

		no =GetComponent<SelectLeft>(); 

	}
	
	public int Att(){
		return attribute;
	}

	public void PersonalData(int number,bool set){
		falseAttribute = data2.Atribute(number);
		attribute1 = right_att.Att();
		attribute2 = center_att.Att();
		if(!set){
			if(attribute == falseAttribute){
				power = data2.AttackData(number);
				hitPoint = data2.HpData(number);
			}else{
				if(falseAttribute == attribute1){
					right_att.ChangeParty(setNo);
					attribute = data2.Atribute(number);
					power = data2.AttackData(number);
					hitPoint = data2.HpData(number);
				}else if(falseAttribute == attribute2){
					center_att.ChangeParty(setNo);
					attribute = data2.Atribute(number);
					power = data2.AttackData(number);
					hitPoint = data2.HpData(number);
				}
			}
		}else{
			power = data2.AttackData(number);
			hitPoint = data2.HpData(number);
			attribute = data2.Atribute(number);
		}
	}

	public void ChangeNo(int number){
		if(setNo==number){
			return;
		}
		//Debug.Log(setNo+":left setNo    "+number+":input  "+attribute+":att");
		PersonalData(number,false);
		//Debug.Log (attribute+":att after left");
		setNo = number;
		data.ChangeLeft(setNo);
		no.ChangeMonster(number);
		switch(number){
		case 1:
			MainSpriteRenaderer.sprite = no1;
			return;
		case 2:
			MainSpriteRenaderer.sprite = no2;
			return;
		case 3:
			MainSpriteRenaderer.sprite = no3;
			return;
		case 4:
			MainSpriteRenaderer.sprite = no4;
			return;
		case 5:
			MainSpriteRenaderer.sprite = no5;;
			return;
		case 6:
			MainSpriteRenaderer.sprite = no6;
			return;
		default:
			return;
		}
	}
	public void ChangeParty(int number){
		Debug.Log(setNo+"left setNo    "+number+"input   "+attribute+"att");
		PersonalData(number,true);
		Debug.Log (attribute+":att after left");
		setNo = number;
		data.ChangeLeft(setNo);
		no.ChangeMonster(number);
		switch(number){
		case 1:
			MainSpriteRenaderer.sprite = no1;
			return;
		case 2:
			MainSpriteRenaderer.sprite = no2;
			return;
		case 3:
			MainSpriteRenaderer.sprite = no3;
			return;
		case 4:
			MainSpriteRenaderer.sprite = no4;
			return;
		case 5:
			MainSpriteRenaderer.sprite = no5;;
			return;
		case 6:
			MainSpriteRenaderer.sprite = no6;
			return;
		default:
			return;
		}
	}
}
