﻿using UnityEngine;
using System.Collections;

public class ClearedTextAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		iTween.ScaleTo (gameObject, iTween.Hash(
			"x", 1,
			"y", 1,
			"time", 1.9f,
			"easetype", iTween.EaseType.easeOutElastic
		));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
