﻿using UnityEngine;
using System.Collections;

public class OneDegreeController : MonoBehaviour {
	private int id;
	private CircularGauge circularGauge;
	private SpriteRenderer spriteRenderer;
	private bool isShow;
	private float alpha;

	// Use this for initialization
	void Start () {
		isShow = false;
		spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
		circularGauge = gameObject.transform.parent.GetComponent<CircularGauge>();
		alpha = spriteRenderer.color.a;
	}
	
	// Update is called once per frame
	void Update () {
		// GetNum()
		if (id < circularGauge.GetValue()) {

			// 表示
			Color color = spriteRenderer.color;
			color.a = alpha;
			spriteRenderer.color = color;
		} else {
			// 非表示
			Color color = spriteRenderer.color;
			color.a = 0;
			spriteRenderer.color = color;
		}
	}

	public void SetId(int num) {
		id = num;
	}
}
