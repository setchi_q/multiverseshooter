﻿using UnityEngine;
using System.Collections;

public class LeadreSelect : MonoBehaviour {

	private GameObject[] attrFrame = new GameObject[3];

	public int position;

	// Use this for initialization
	void Start () {
		attrFrame[0] = GameObject.Find ("PartyMonster_Left");
		attrFrame[1] = GameObject.Find ("PartyMonster_Center");
		attrFrame[2] = GameObject.Find ("PartyMonster_Right");
	}
	
	// Update is called once per frame
	void Update () {
	}
	void ChangePosition(int position) {
		//Debug.Log (position.ToString() + ", " + this.position.ToString());
		if (this.position == position)
			return;
		
		iTween.ScaleTo (attrFrame[this.position], iTween.Hash ("x", 0.9f, "y", 0.9f, "time", 0.4f, "easetype", iTween.EaseType.easeOutElastic));
		this.position = position;
		iTween.ScaleTo (attrFrame[this.position], iTween.Hash ("x", 1.2f ,"y", 1.2f, "time", 0.4f, "easetype", iTween.EaseType.easeOutElastic));
	}
}
