﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public float speed;
	public int power;

	public AudioClip soundEffectClip;

	private int bounceCount = 0;
	private int attribute;

	void Start() {
		AudioSource.PlayClipAtPoint (soundEffectClip, new Vector3(0, 1, -10));
	}

	public void SetAttribute(int x) {
		attribute = x;
	}

	public int GetAttribute() {
		return attribute;
	}

	void OnTriggerExit2D (Collider2D c){
		bounceCount += 1;
		if(bounceCount == 3){
			Destroy(gameObject);
		}
	}
}