﻿using UnityEngine;
using System.Collections;

public class TheDigit : MonoBehaviour {
	SpriteRenderer MainSpriteRenaderer;

	public Sprite No0;
	public Sprite No1;
	public Sprite No2;
	public Sprite No3;
	public Sprite No4;
	public Sprite No5;
	public Sprite No6;
	public Sprite No7;
	public Sprite No8;
	public Sprite No9;

	public bool flag;

	// Use this for initialization
	void Start () {
		MainSpriteRenaderer = gameObject.GetComponent<SpriteRenderer>();
		//MainSpriteRenaderer.sprite = No0;
		render();
	}

	void render(){
		if(!flag){
			this.renderer.enabled = false;
		}
	}


	public void ChangeSprite(int number){
		if(number>9){
			this.renderer.enabled = false;
			return;
		}else{
			this.renderer.enabled = true;
		}
		switch(number){
		case 0:
			MainSpriteRenaderer.sprite = No0;
			return;
		case 1:
			MainSpriteRenaderer.sprite = No1;
			return;
		case 2:
			MainSpriteRenaderer.sprite = No2;
			return;
		case 3:
			MainSpriteRenaderer.sprite = No3;
			return;
		case 4:
			MainSpriteRenaderer.sprite = No4;
			return;
		case 5:
			MainSpriteRenaderer.sprite = No5;;
			return;
		case 6:
			MainSpriteRenaderer.sprite = No6;
			return;
		case 7:
			MainSpriteRenaderer.sprite = No7;
			return;
		case 8:
			MainSpriteRenaderer.sprite = No8;
			return;
		case 9:
			MainSpriteRenaderer.sprite = No9;
			return;
		default:
			return;
		}
	}
}
