﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	public float initialVelocity; // 適当
	public GameObject waterBullet;
	public GameObject fireBullet;
	public GameObject windBullet;

	private GameObject[] attrButton = new GameObject[3];
	private SpriteRenderer[] attrButtonSpriteRenderer = new SpriteRenderer[3];
	private Button[] button = new Button[3];

	// コンボ管理
	private float lastAttackedTime = 0;
	private float comboEnableTime = 3f;
	private int combo = 0;

	private int currentAttribute;
	private float gameEndDelay;
	private bool gameEnd;

	void Start () {
		GameObject leftButton = GameObject.Find ("left");
		GameObject centerButton = GameObject.Find ("center");
		GameObject rightButton = GameObject.Find ("right");

		attrButton [leftButton.GetComponent<Button> ().GetAttribute ()] = leftButton;
		attrButton [centerButton.GetComponent<Button> ().GetAttribute ()] = centerButton;
		attrButton [rightButton.GetComponent<Button> ().GetAttribute ()] = rightButton;

		for (int i = 0; i < attrButton.Length; i++) {
			attrButtonSpriteRenderer[i] = attrButton[i].GetComponent<SpriteRenderer>();
			button[i] = attrButton[i].GetComponent<Button>();
		}
		currentAttribute = 1;
	}

	void Update () {
		if (gameEnd) {
			gameEndDelay += Time.deltaTime;
			if (gameEndDelay > 1.5f) {
				Application.LoadLevel ("Result");
			}
		}
		// comboReset
		if (combo != 0 && Time.time - lastAttackedTime > comboEnableTime) {
			combo = 0;
		}
	}
	
	public void Shoot(Vector3 direction, Vector3 currentScreenPoint) {
		if (button[currentAttribute].IsCoolTimeDuring()) {
			return;
		}
		
		GameObject bulletObjet = GetBulletByAttribute(currentAttribute);
		GameObject bullet = (GameObject)Instantiate(bulletObjet, transform.position, transform.rotation) as GameObject;
		bullet.transform.position = Camera.main.ScreenToWorldPoint(currentScreenPoint);
		bullet.rigidbody2D.AddForce(direction * initialVelocity, ForceMode2D.Impulse);
		Bullet x = bullet.GetComponent<Bullet>();
		x.SetAttribute(currentAttribute);

		button[currentAttribute].OneConsumesBullet();
	}
	
	GameObject GetBulletByAttribute(int attribute) {
		switch (attribute) {
		case 0:
			return fireBullet;
		case 1:
			return waterBullet;
		case 2:
			return windBullet;
		default:
			return fireBullet;
		}
	}

	public void ChangeCurrentAttribute(int attribute) {
		if (this.currentAttribute == attribute)
			return;
		
		iTween.ScaleTo (attrButton[this.currentAttribute], iTween.Hash ("x", 1f, "y", 1f, "time", 0.4f, "easetype", iTween.EaseType.easeOutElastic));
		this.currentAttribute = attribute;
		iTween.ScaleTo (attrButton[this.currentAttribute], iTween.Hash ("x", 1.4f ,"y", 1.4f, "time", 0.4f, "easetype", iTween.EaseType.easeOutElastic));
	}

	public void AttackSuccess() {
		// コンボ数制御
		combo++;
		lastAttackedTime = Time.time;
	}

	public int GetComboNum() {
		return combo;
	}

	public void GameEnd() {
		gameEnd = true;
	}
}
