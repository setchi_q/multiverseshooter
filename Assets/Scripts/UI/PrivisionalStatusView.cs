using UnityEngine;
using System.Collections;

public class PrivisionalStatusView : MonoBehaviour {
	GameController gameController;
	private GUIStyle style;

	// Use this for initialization
	void Start () {
		style = new GUIStyle();
		style.fontSize = 30;

		gameController = GameObject.Find ("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {
		DispComboNum ();
	}

	public void DispComboNum() {
		int combo = gameController.GetComboNum ();
		string text = combo == 0 ? "" : combo.ToString () + " Combo!!";
		style.richText = true;
		GUI.Label (new Rect (20, 100, 100, 30),"<color=white>" +  text + "</color>", style);
	}
}
