﻿using UnityEngine;
using System.Collections;

public class BackMenu : MonoBehaviour {

	private bool flag; 
	private float timer;
	public GameObject touchAudio;
	private TouchAudio touch;

	void Start(){
		touch = touchAudio.GetComponent<TouchAudio>();
		flag = false;
	}

	void Update() {
		if(flag){
			timer += Time.deltaTime;
			if(timer >0.3f){
				Changescene();
			}
		}
	}

	void Changescene(){
		Application.LoadLevel("Main");
	}

	void OnMouseDown() {
		touch.touch();
		timer = 0;
		flag = true;
	}
}
