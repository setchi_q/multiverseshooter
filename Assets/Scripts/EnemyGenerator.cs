﻿using UnityEngine;
using System.Collections;

public class EnemyGenerator : MonoBehaviour {

	public float enemyPopInterval;
	public GameObject[] enemiesOfFire;
	public GameObject[] enemiesOfWater;
	public GameObject[] enemiesOfWind;
	public GameObject boss;

	private float timer;

	private int currentAttribute;
	private int enemyNum = 20;
	private int enemyCount = 0;

	private float popBossAnimationCount;
	private GameObject bossPopEffect;
	public GameObject warning;
	public GameObject bossPopEffectBack;

	private enum StageState {
		Early,
		PopBoss,
		BossFight,
		Climax
	}
	private StageState stageState;

	private AudioSource bgm1;
	public AudioClip bossBgmClip;

	void Start () {
		currentAttribute = 0;
		popBossAnimationCount = 0;
		stageState = StageState.Early;

		bgm1 = Camera.main.gameObject.GetComponent<AudioSource>();
		bossPopEffect = GameObject.Find ("BossPopEffect");
		bossPopEffect.SetActive (false);
	}

	public void Update () {

		switch (stageState) {
		case StageState.Early:
			timer += Time.deltaTime;
			if (timer < enemyPopInterval) {
				return;
			}
			timer = 0;

			PopEnemy(GetNextEnemy());

			enemyCount++;
			if (enemyCount > enemyNum) {
				stageState = StageState.PopBoss;
				// アニメーションスタート
				BossPopEffectAnimationStart();
			}
			break;

		case StageState.PopBoss:
			popBossAnimationCount += Time.deltaTime;
			Debug.Log (popBossAnimationCount);

			if (popBossAnimationCount < 3.0f) {
				bgm1.volume = Mathf.Lerp( 1, 0.0f, popBossAnimationCount / 3 );

			} else {
				// 再生
				bgm1.clip = bossBgmClip;
				bgm1.volume = 1;
				bgm1.Play ();
				PopEnemy(boss);
				bossPopEffect.SetActive(false);
				stageState = StageState.BossFight;
			}
			break;

		case StageState.BossFight:
			break;

		case StageState.Climax:
			break;
		}
	}

	void BossPopEffectAnimationStart() {
		bossPopEffect.SetActive(true);
		iTween.ScaleTo (bossPopEffect, iTween.Hash (
			"y", 1,
			"x", 1,
			"time", 0.4f,
			"oncomplete", "Test",
			"oncompletetarget", gameObject
		));
	}

	void Test() {
		iTween.ScaleTo (gameObject, iTween.Hash (
			"x", 1,
			"time", 1f,
			"oncomplete", "Test2",
			"oncompletetarget", gameObject
		));
	}

	void Test2() {
		iTween.ValueTo (gameObject, iTween.Hash (
			"from", new Vector2(0, 0),
			"to", new Vector2(1, 1),
			"time", 0.6f,
			"easetype", iTween.EaseType.easeOutQuart,
			"onupdate", "BossEffectAnimateOnUpdate",
			"onupdatetarget", gameObject
			));
		iTween.ScaleTo (bossPopEffect, iTween.Hash("y", 0, "time", 0.6f, "easetype", iTween.EaseType.easeOutQuart));
	}

	void BossEffectAnimateOnUpdate(Vector2 value) {
		SpriteRenderer warningSpriteRenderer = warning.GetComponent<SpriteRenderer> ();
		Vector3 localScale = warning.transform.localScale;
		localScale.x = (1 + value.x) * 2.5f;
		warning.transform.localScale = localScale;
		Color color = warningSpriteRenderer.color;
		color.a = (1 - value.x);
		warningSpriteRenderer.color = color;
	}

	public void PopEnemyByAttribute(int attribute) {
		PopEnemy (GetRandomEnemyByAttribute (attribute));
	}

	GameObject GetNextEnemy() {
		currentAttribute = --currentAttribute < 0 ? 2 : currentAttribute;
		return GetRandomEnemyByAttribute (currentAttribute);
	}
	
	void PopEnemy(GameObject enemy) {
		float offsx = Random.Range (-3, 3);
		float offsy = Random.Range (5, 6);
		Instantiate (enemy, transform.position + new Vector3 (offsx, offsy, 0), transform.rotation);
	}
	
	GameObject GetRandomEnemyByAttribute(int attribute) {
		GameObject[] enemyList;
		
		switch (attribute) {
		case 0:
			enemyList = enemiesOfFire;
			break;
		case 1:
			enemyList = enemiesOfWater;
			break;
		case 2:
			enemyList = enemiesOfWind;
			break;
		default:
			enemyList = enemiesOfFire;
			break;
		}
		return enemyList[Mathf.RoundToInt(Random.Range(0, enemyList.Length - 1))];
	}
}
