﻿using UnityEngine;
using System.Collections;

public class LeaderCheck : MonoBehaviour {

	public GameObject leaderCheck;
	public int position;//left = 0,center = 1,right = 2

	private float timer;
	private int count = 0;

	void Start(){
		timer = 0;
		count = 0;
	}

	void Update(){
		timer += Time.deltaTime;
		if(timer < 0.25f && count >= 2){
			leaderCheck.SendMessage ("ChangePosition", position);
			//Debug.Log(count);
			count = 0;
			//Debug.Log(count);
		}
		if(count >= 1){
			if(timer > 0.25f){
				count = 0;
			}
		}
	}

	void OnMouseDown (){
		timer = 0;
		count += 1;
	}

}
