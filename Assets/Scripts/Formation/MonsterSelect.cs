using UnityEngine;
using System.Collections;

public class MonsterSelect : MonoBehaviour {

	public int monsterNo;

	public GameObject mname;
	public GameObject monster;
	public GameObject skill;
	public GameObject LeaderSkill;

	private ChangeMonsterStatus c_name;
	private ChangeMonsterStatus c_mons;
	private ChangeMonsterStatus c_skill;
	private ChangeMonsterStatus c_lead;

	private GameObject left;
	private GameObject center;
	private GameObject right;

	public GameObject circle;

	private bool isTouch = false;
	private Vector3 selfPoint;
	private Vector3 startPos;
	private Vector3 endPos;

	private GameObject circle1;

	private float x1 = -1.6f;
	private float x2 = -0.275f;
	private float x3 = 1.6f;
	private float x4 = 3.0f;
	private float y1 = 5.5f;
	private float y2 = 4.0f;

	private GameObject touchAudio;
	private TouchAudio touch;

	// Use this for initialization
	void Start () {
		touchAudio = GameObject.Find("touchAudio2");
		if(touchAudio){
			touch = touchAudio.GetComponent<TouchAudio>();
		}
		left = GameObject.Find("PartyMonster_Left");
		center = GameObject.Find("PartyMonster_Center");
		right = GameObject.Find("PartyMonster_Right");
		c_name = mname.GetComponent<ChangeMonsterStatus>();
		c_mons = monster.GetComponent<ChangeMonsterStatus>();
		c_lead = LeaderSkill.GetComponent<ChangeMonsterStatus>();
		c_skill = skill.GetComponent<ChangeMonsterStatus>();
	}
	private Vector3 screenPoint;
	private Vector3 offset;
	private Vector3 currentPosition;
	
	void OnMouseDown()
	{
		isTouch = true;
		c_lead.ChangeNO(monsterNo);
		c_mons.ChangeNO(monsterNo);
		c_name.ChangeNO(monsterNo);
		c_skill.ChangeNO(monsterNo);
		//カメラから見たオブジェクトの現在位置を画面位置座標に変換
		screenPoint = Camera.main.WorldToScreenPoint(transform.position);
		
		//取得したscreenPointの値を変数に格納
		float x = Input.mousePosition.x;
		float y = Input.mousePosition.y;
		
		//オブジェクトの座標からマウス位置(つまりクリックした位置)を引いている。
		//これでオブジェクトの位置とマウスクリックの位置の差が取得できる。
		//ドラッグで移動したときのずれを補正するための計算だと考えれば分かりやすい
		offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(x, y, screenPoint.z));
	}
	
	void OnMouseDrag()
	{
		//ドラッグ時のマウス位置を変数に格納
		float x = Input.mousePosition.x;
		float y = Input.mousePosition.y;

		//ドラッグ時のマウス位置をシーン上の3D空間の座標に変換する
		Vector3 currentScreenPoint = new Vector3(x, y, screenPoint.z);
		
		//クリックした場所の差から、オブジェクトを移動する座標位置を求める
		currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint) + offset;
		
		//オブジェクトの位置を変更する
		//transform.position = currentPosition;
		if(circle1){
			circle1.transform.position = currentPosition;
		}
	}
	void Update () {
		if (isTouch && Input.GetMouseButtonDown (0)) {
			left = GameObject.Find("PartyMonster_Left");
			center = GameObject.Find("PartyMonster_Center");
			right = GameObject.Find("PartyMonster_Right");
			startPos = Input.mousePosition;
			selfPoint = Camera.main.WorldToScreenPoint(startPos);
			circle1 = Instantiate(circle,transform.position,transform.rotation) as GameObject;
		}else if (isTouch && Input.GetMouseButtonUp (0)) {
			if(touchAudio){
				touch.touch();
			}
			Destroy(circle1);
			endPos = Input.mousePosition;
			Vector3 mouseUpWorldPoint = Camera.main.ScreenToWorldPoint(new Vector3(
				endPos.x,
				endPos.y,
				selfPoint.z
				));
			if((x1 <= mouseUpWorldPoint.x)&&(x2 > mouseUpWorldPoint.x)){
				if((y2 <= mouseUpWorldPoint.y)&&(y1 >= mouseUpWorldPoint.y)){
					left.SendMessage ("ChangeNo", monsterNo);
				}
			}else if((x2 <= mouseUpWorldPoint.x)&&(x3 > mouseUpWorldPoint.x)){
				if((y2 <= mouseUpWorldPoint.y)&&(y1 > mouseUpWorldPoint.y)){
					center.SendMessage ("ChangeNo", monsterNo);
				}
			}else if((x3 <= mouseUpWorldPoint.x)&&(x4 > mouseUpWorldPoint.x)){
				if((y2 <= mouseUpWorldPoint.y)&&(y1 > mouseUpWorldPoint.y)){
					right.SendMessage ("ChangeNo", monsterNo);
				}
			}
			isTouch = false;
		}
	}
}
