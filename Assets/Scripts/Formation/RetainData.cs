﻿using UnityEngine;
using System.Collections;

public class RetainData : MonoBehaviour {
	public GameObject parent;
	public GameObject child1;
	public GameObject child2;
	public GameObject child3;
	private GameObject child4;
	private GameObject child5;
	private GameObject child6;
	private GameObject child7;
	private GameObject child8;
	private GameObject child9;
	private GameObject child10;
	private GameObject child11;

	static public int count = 0;
	private static bool flag = false;
	
	void Start () {
		child4 = GameObject.Find("digit1");
		child5 = GameObject.Find("digit10");
		child6 = GameObject.Find("digitleft1");
		child7 = GameObject.Find("digitleft10");
		child8 = GameObject.Find("digitcenter1");
		child9 = GameObject.Find("digitcenter10");
		child10 = GameObject.Find("digitright1");
		child11 = GameObject.Find("digitright10");
		count = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if(count == 1 && Application.loadedLevelName!="Formation"){
			child1.SetActive(false);
			child2.SetActive(false);
			child3.SetActive(false);
			child4.renderer.enabled = false;
			child5.renderer.enabled = false;
			child6.renderer.enabled = false;
			child7.renderer.enabled = false;
			child8.renderer.enabled = false;
			child9.renderer.enabled = false;
			child10.renderer.enabled = false;
			child11.renderer.enabled = false;
			count = 0;

		}
		if(count == 0 && Application.loadedLevelName=="Formation"){
			child1.SetActive(true);
			child2.SetActive(true);
			child3.SetActive(true);
			child4.renderer.enabled = true;
			child5.renderer.enabled = true;
			child6.renderer.enabled = true;
			child7.renderer.enabled = true;
			child8.renderer.enabled = true;
			child9.renderer.enabled = true;
			child10.renderer.enabled = true;
			child11.renderer.enabled = true;
			count = 1;
		}
	}
	void Awake() {
		if(!flag){
			DontDestroyOnLoad(parent);
			flag = true;
		}else{
			Destroy(this.gameObject);
		}
	}
}
