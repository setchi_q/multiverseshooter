﻿using UnityEngine;
using System.Collections;

public class BackWindow : MonoBehaviour {

	public GameObject stage;
	public GameObject Friend_menu;

	private GameObject target;
	private TouchAudio touch;

	void Start(){
		target = GameObject.Find("touchAudio");
		if(target){
			touch = target.GetComponent<TouchAudio>();
		}
	}

	void OnMouseDown(){
		if(target){
			touch.touch();
		}
		stage.SetActive(true);
		Friend_menu.SetActive(false);
	}
}
