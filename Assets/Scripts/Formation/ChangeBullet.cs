﻿using UnityEngine;
using System.Collections;

public class ChangeBullet : MonoBehaviour {
	
	private int left;
	private int center;
	private int right;
	private int bullet;

	private int leftBullet;
	private int centerBullet;
	private int rightBullet;

	private PowerData data;
	private int[] digit = new int[9];
	private TheDigit[] x = new TheDigit[9]; 
	//private bool flag = false;
	
	public GameObject digit1;
	public GameObject digit10;
	public GameObject digitLeft1;
	public GameObject digitLeft10;
	public GameObject digitCenter1;
	public GameObject digitCenter10;
	public GameObject digitRight1;
	public GameObject digitRight10;

	private int count = 0;

	private bool flag1;
	private bool flag2;
	private bool flag3;
	
	void Start () {
		left = 1;
		center = 2;
		right = 3;
		data = GetComponent<PowerData>();
		x[0] = digit1.GetComponent<TheDigit>();
		x[1] = digit10.GetComponent<TheDigit>();
		x[2] = digitLeft1.GetComponent<TheDigit>();
		x[3] = digitLeft10.GetComponent<TheDigit>();
		x[4] = digitCenter1.GetComponent<TheDigit>();
		x[5] = digitCenter10.GetComponent<TheDigit>();
		x[6] = digitRight1.GetComponent<TheDigit>();
		x[7] = digitRight10.GetComponent<TheDigit>();
	}

	void Update(){
		if(count == 0 && Application.loadedLevelName!="Formation"){
			count = 1;
		}
		if(count == 1 && Application.loadedLevelName=="Formation"){
			data = GetComponent<PowerData>();
			x[0] = digit1.GetComponent<TheDigit>();
			x[1] = digit10.GetComponent<TheDigit>();
			x[2] = digitLeft1.GetComponent<TheDigit>();
			x[3] = digitLeft10.GetComponent<TheDigit>();
			x[4] = digitCenter1.GetComponent<TheDigit>();
			x[5] = digitCenter10.GetComponent<TheDigit>();
			x[6] = digitRight1.GetComponent<TheDigit>();
			x[7] = digitRight10.GetComponent<TheDigit>();
			count = 0;
			Debug.Log("testde");
		}
	}

	public void ChangeLeft(int number){
		left = number;
		Bulletrest();
	}
	
	public void ChangeCenter(int number){
		center = number;
		Bulletrest();
	}
	
	public void ChangeRight(int number){
		right = number;
		Bulletrest();
	}
	
	void Bulletrest(){
		bullet = 0;
		leftBullet = data.BulletRemaining(left);
		if (leftBullet>=10){
			flag1 = true;
		}else{
			flag1 =false;
		}
		centerBullet = data.BulletRemaining(center);
		if (centerBullet>=10){
			flag2 = true;
		}else{
			flag2 =false;
		}
		rightBullet = data.BulletRemaining(right);
		if (rightBullet>=10){
			flag3 = true;
		}else{
			flag3 =false;
		}
		bullet = leftBullet + centerBullet + rightBullet;
		Cast(bullet);
		CastLeft();
		CastCenter();
		CastRight();
		Output();
	}
	
	void Cast(int totalHP){
		digit[0] = totalHP % 10;
		totalHP = (totalHP-digit[0]) / 10;
		digit[1] = totalHP % 10;
	}
	void CastLeft(){
		digit[2] = leftBullet % 10;
		leftBullet = (leftBullet-digit[2]) / 10;
		if(flag1){
			digit[3] = leftBullet % 10;
		}
	}
	void CastCenter(){
		digit[4] = centerBullet % 10;
		centerBullet = (centerBullet-digit[4]) / 10;
		if(flag2){
			digit[5] = centerBullet % 10;
		}
	}
	void CastRight(){
		digit[6] = rightBullet % 10;
		rightBullet = (rightBullet-digit[6]) / 10;
		if(flag3){
			digit[7] = rightBullet % 10;
		}
	}
	
	void Output(){
		x[0].ChangeSprite(digit[0]);
		x[1].ChangeSprite(digit[1]);

		x[2].ChangeSprite(digit[2]);
		if(flag1){
			x[3].ChangeSprite(digit[3]);
		}else{
			x[3].ChangeSprite(10);
		}

		x[4].ChangeSprite(digit[4]);
		if(flag2){
			x[5].ChangeSprite(digit[5]);
		}else{
			x[5].ChangeSprite(10);
		}

		x[6].ChangeSprite(digit[6]);
		if(flag3){
			x[7].ChangeSprite(digit[7]);
		}else{
			x[7].ChangeSprite(10);
		}
	}
}