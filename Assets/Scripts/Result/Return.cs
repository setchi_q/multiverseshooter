﻿using UnityEngine;
using System.Collections;

public class Return : MonoBehaviour {

	private float timer;
	private float interval = 1.0f;
	private int count;
	
	// Use this for initialization
	void Start () {
		timer = 0;
		count = 0;
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if(timer > interval){
			count += 1;
			timer = 0;
			if(count == 4){
				Application.LoadLevel("Main");
			}
		}
	}
	
	void OnMouseDown(){
		timer += interval;
	}
}
