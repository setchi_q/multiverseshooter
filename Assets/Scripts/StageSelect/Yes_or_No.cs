﻿using UnityEngine;
using System.Collections;

public class Yes_or_No : MonoBehaviour {

	public bool flag;
	public GameObject Friend_menu;
	public GameObject stage;

	private ChangeScene scenes;

	private int stageNo;
	private Friend friend;

	private GameObject target;
	private TouchAudio touch;


	void Start(){
		target = GameObject.Find("touchAudio");
		if(target){
			touch = target.GetComponent<TouchAudio>();
		}
		friend = Friend_menu.GetComponent<Friend>();
		stageNo = friend.OutputStage();
		scenes = gameObject.GetComponent<ChangeScene>();
		Debug.Log(stageNo);
	}


	void OnMouseDown(){
		if(target){
			touch.touch();
		}
		if(flag){
			stage.SetActive(true);
			scenes.Change_Scene(stageNo);
			Friend_menu.SetActive(false);
		}else{
			stage.SetActive(true);
			Friend_menu.SetActive(false);
		}
	}

}
