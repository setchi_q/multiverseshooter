﻿using UnityEngine;
using System.Collections;

public class FriendSelect : MonoBehaviour {

	//private GameObject Parent;
	public GameObject Question;
	//public GameObject parent;
	public GameObject Friend_Menu;
	public GameObject select;
	public int fMonsterNo;

	private Friend friend;
	private GameObject target;

	private DataOutPut data;
	private ChangeFlashing flash;

	public GameObject touchAudio;
	private TouchAudio touch;

	// Use this for initialization
	void Start () {
		touch = touchAudio.GetComponent<TouchAudio>();
		flash = select.GetComponent<ChangeFlashing>();
		friend = Friend_Menu.GetComponent<Friend>();
		//Parent = gameObject.transform.parent.gameObject;
		target = GameObject.Find("Party");
		
		if(target){
			data = target.GetComponent<DataOutPut>();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown(){
		touch.touch ();
		flash.Flashing();
		friend.ChangeFirend(fMonsterNo);
		if(target){
			data = target.GetComponent<DataOutPut>();
			data.ChangeFriend(fMonsterNo);
		}
		Question.SetActive(true);
		//Parent.SetActive(false);

	}
}
