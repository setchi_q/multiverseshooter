﻿using UnityEngine;
using System.Collections;

public class test5 : MonoBehaviour {
	public int attribute;
	public GameObject remainingBullet;
	
	private GameController gameController;
	private SpriteRenderer spriteRenderer;
	private GUIText remainingBulletGUIText;
	
	private static int bulletNum = 5;
	private int remainingBulletNum = bulletNum;
	
	private int bulletShootCount = 0;
	private int skillCondition = bulletNum * 3;
	
	private static int coolTime = 5;
	private float coolStartTime = -coolTime;
	
	private int flashingType = 0;
	
	private GameObject party;
	private DataOutPut dataOutPut;
	public PowerData powerData;
	public int position;//left = 0 center = 1 right = 2
	private int monsterNo;
	
	
	void Start() {
		powerData = GetComponent<PowerData>();
		party = GameObject.Find ("Party");
		if(party){
			dataOutPut = party.GetComponent<DataOutPut>();
			switch(position){
			case 0:
				monsterNo = dataOutPut.OutputLeft();
				break;
			case 1:
				monsterNo = dataOutPut.OutputCenter();
				break;
			case 2:
				monsterNo = dataOutPut.OutputRight();
				break;
			default:
				monsterNo = dataOutPut.OutputLeft();
				break;
			}
			bulletNum = powerData.BulletRemaining(monsterNo);
			attribute = powerData.Atribute(monsterNo);
		}
		
		gameController = GameObject.Find ("GameController").GetComponent<GameController> ();
		spriteRenderer = GetComponent<SpriteRenderer>();
		remainingBulletGUIText = remainingBullet.GetComponent<GUIText>();
		SetRemainingNumText (5);
	}
	
	void Update() {
		UpdateBulletCoolTime ();
	}
	
	void OnMouseDown() {
		gameController.ChangeCurrentAttribute (attribute);
	}
	
	public void ButtonFlashing() {
		Hashtable iTweenFlashingParams = new Hashtable ();
		switch (flashingType) {
		case 0:
			iTweenFlashingParams.Add ("from", new Vector2(1, 0));
			iTweenFlashingParams.Add ("to", new Vector2(0, 0));
			iTweenFlashingParams.Add ("time", 0.7f);
			break;
		case 1:
			iTweenFlashingParams.Add ("from", new Vector2(0, 0));
			iTweenFlashingParams.Add ("to", new Vector2(1, 0));
			iTweenFlashingParams.Add ("time", 0.5f);
			break;
		case 2:
			iTweenFlashingParams.Add ("from", new Vector2(0, 0));
			iTweenFlashingParams.Add ("to", new Vector2(1, 0));
			iTweenFlashingParams.Add ("time", 0.5f);
			break;
		}
		iTweenFlashingParams.Add ("onupdate", "ChangeOpacity");
		iTweenFlashingParams.Add ("oncomplete", "ButtonFlashing");
		iTweenFlashingParams.Add ("oncompletetarget", gameObject);
		iTweenFlashingParams.Add ("onupdatetarget", gameObject);
		
		iTween.ValueTo (gameObject, iTweenFlashingParams);
	}
	
	void ChangeOpacity(Vector2 value) {
		Color color = spriteRenderer.color;
		color.a = value.x;
		if (attribute == 2) {
			color.a = Mathf.Abs(value.x - 0.5f) * 2;
		}
		spriteRenderer.color = color;
	}
	
	void RemainingBulletAnimationOnUpdate(Vector2 value) {
		remainingBulletGUIText.fontSize = Mathf.RoundToInt(value.x);
		remainingBulletGUIText.pixelOffset = new Vector2 (-(value.x - 60), (value.x - 60)) / 2;
		
		Color color = remainingBulletGUIText.color;
		color.a = 1 - (value.x - 60) / 240;
		remainingBulletGUIText.color = color;
	}
	
	void SetRemainingBullet(int remainingNum) {
		this.remainingBulletNum = remainingNum;
	}
	
	public void OneConsumesBullet() {
		bulletShootCount++;
		
		SetRemainingBullet (remainingBulletNum - 1);
		SetRemainingNumText (remainingBulletNum);
		if (this.remainingBulletNum == 0) {
			// クールダウン開始
			coolStartTime = Time.time;
			SetRemainingBullet(bulletNum);
		}
		
		if (bulletShootCount > skillCondition) {
			// スキル発射可能状態
			bulletShootCount = 0;
			ButtonFlashing ();
		}
	}
	
	public void SetRemainingNumText(int num) {
		if (remainingBulletGUIText.text == num.ToString()) return;
		iTween.ValueTo(gameObject, iTween.Hash("from", new Vector2(300, 300), "to", new Vector2(60, 60), "time", 0.6f, "onUpdate", "RemainingBulletAnimationOnUpdate", "onUpdateTarget", gameObject, "easeType", iTween.EaseType.easeOutExpo));  
		remainingBulletGUIText.text = num.ToString();
	}
	
	void UpdateBulletCoolTime() {
		if (coolStartTime == -5) {
			return;
		}
		float elapsedTime = Time.time - coolStartTime;
		UpdateCoolTimeUI(elapsedTime / coolTime);
		if (elapsedTime > coolTime) {
			// クールタイムリセット
			coolStartTime = -5;
			SetRemainingNumText(bulletNum);
		}
	}
	
	void UpdateCoolTimeUI(float value) {
		gameObject.GetComponentInChildren<CircularGauge>().SetValue(1 - value);
	}
	
	public bool IsCoolDown() {
		return Time.time - coolStartTime < coolTime;
	}
}
