﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {

	private bool isGame = true;
	private bool isPause = false;
	private bool isRetire = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){

		int sw = Screen.width;
		int sh = Screen.height;

		if(isGame){
			if(GUI.Button(new Rect(10,10,sw/8,sw/8),"| |")){
				Time.timeScale = 0;
				isPause = true;
				isGame = false;
			}
		}

		if(isPause){
			GUI.Box(new Rect(5,sh/5,sw-10,sh*3/5),"Menu");
			GUI.Label(new Rect(sw/3,sh*15/50,sw*3/5,sh/2-10)," The Dungeon Name ");

			if(GUI.Button(new Rect(sw/3,sh*30/50,sw/3,sh*3/50),"リタイア")){
				isPause = false;
				isRetire = true;
			}
			if(GUI.Button(new Rect(sw/3,sh*35/50,sw/3,sh*3/50)," もどる ")){
				Time.timeScale = 1;
				isPause = false;
				isGame = true;
			}
		}
		if(isRetire){

			GUI.Box(new Rect(5,sh*2/5,sw-10,sh*2/7),"リタイア");
			GUI.Label(new Rect(sw/5,sh*23/50,sw*3/5,sh/2-10),"このダンジョンで取得した取得した経験値、アイテムを失いますがよろしいですか？");

			if(GUI.Button(new Rect(sw*15/100,sh*30/50,sw*3/10,sh*3/50),"はい")){
				Time.timeScale = 1;
				Application.LoadLevel("Main");
			}
			if(GUI.Button(new Rect(sw*55/100,sh*30/50,sw*3/10,sh*3/50),"いいえ")){
				isPause = true;
				isRetire = false;

			}
		}
	}
}
