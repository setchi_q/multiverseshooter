﻿using UnityEngine;
using System.Collections;

public class ChangeScene : MonoBehaviour {

	public GameObject stage1;
	public GameObject stage2;

	private Village village;
	private Forest forest;

	void Start(){
		village = stage1.GetComponent<Village>();
		forest = stage2.GetComponent<Forest>();
	}
	
	public void Change_Scene(int stageNo){
		switch(stageNo){
		case 1:
			village.Stage();
			return;
		case 2:
			forest.Stage();
			return;
		default:
			return;
		}
	}

}
