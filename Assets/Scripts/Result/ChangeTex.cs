﻿using UnityEngine;
using System.Collections;

public class ChangeTex : MonoBehaviour {

	SpriteRenderer MainSpriteRenaderer;

	private float timer;
	private float interval = 0.3f;
	private bool flag = false;

	public Sprite no1;
	public Sprite no2;
	public Sprite no3;

	private GameObject touchAudio;
	private TouchAudio touch;

	// Use this for initialization
	void Start () {
		touchAudio = GameObject.Find("touchAudio");
		if(touchAudio){
			touch = touchAudio.GetComponent<TouchAudio>();
		}
		timer = 0;
		MainSpriteRenaderer = gameObject.GetComponent<SpriteRenderer>();
		MainSpriteRenaderer.sprite = no1;

	}

	void Update(){
		timer += Time.deltaTime;
		if(flag==true && timer > interval){
			Debug.Log("test");
			Changetex2();
		}
	}
	
	void Changetex(){
		if(touchAudio){
			touch.touch();
		}
		MainSpriteRenaderer.sprite = no2;
		timer = 0;
		flag = true;
	}

	void Changetex2(){
		MainSpriteRenaderer.sprite = no3;
	}
}
