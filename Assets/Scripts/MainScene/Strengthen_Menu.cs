﻿using UnityEngine;
using System.Collections;

public class Strengthen_Menu : MonoBehaviour {
	public GameObject menu;
	private int count = 0;
	public GameObject touchAudio;
	private TouchAudio touch;

	void Start(){
		touch = touchAudio.GetComponent<TouchAudio>();
	}

	void OnMouseDown(){
		touch.touch();
		if(count == 0){
			iTween.MoveTo(menu, iTween.Hash("x",-2.3, "islocal", true));
			count += 1;
		}
	}
	void OnMouseEnter(){
		if(count == 0){
			iTween.MoveTo(menu, iTween.Hash("x",-2.3, "islocal", true));
			count += 1;
		}
	}
	void OnMouseExit(){
		if(count == 1){
			iTween.MoveTo(menu, iTween.Hash("x", -3, "islocal", true));
			count -= 1;
		}
	}
	void OnMouseUp(){
		if(count == 1){
			iTween.MoveTo(menu, iTween.Hash("x", -3, "islocal", true));
			count -= 1;
		}
	}
}