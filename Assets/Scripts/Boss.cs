﻿using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour {
	public float speed;
	public GameObject destroy;
	public GameObject hitEffect;
	public GameObject enemyAttackEffect;
	public float HP;
	public int currentAttribute;
	public int enemyPower;
	
	public Vector2 speed2 = new Vector2(0.05f, 0.05f);
	
	private GameController gameController;
	private EnemyGenerator enemyGenerator;
	private SpriteRenderer spriteRenderer;
	private ParticleSystem particleSystem;
	
	private float timer = 0;
	private float attributeChangeInterval = 6f;
	
	private HPGauge hpGauge;
	private float maxHP;
	
	private bool die = false;
	
	void Start () {
		maxHP = HP;
		SetDirection ();
		gameController = GameObject.Find ("GameController").GetComponent<GameController>();
		enemyGenerator = GameObject.Find ("EnemyGenerateArea").GetComponent<EnemyGenerator>();
		hpGauge = GetComponentInChildren<HPGauge>();
		spriteRenderer = GetComponent<SpriteRenderer>();
		particleSystem = GetComponent<ParticleSystem>();
		EnemyScaleAnimation ();
		SetAttribute (0);
		Move ();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (timer > attributeChangeInterval) {
			SetAttribute( (1 + currentAttribute) % 3 );
			for (int i = 0; i < 3; i++) {
				enemyGenerator.PopEnemyByAttribute(currentAttribute + 1  % 3);
			}
			timer = 0;
		}
	}
	
	void SetAttribute(int attribute) {
		iTween.ValueTo (gameObject, iTween.Hash(
			"from", GetColorVectorByAttribute(currentAttribute),
			"to", GetColorVectorByAttribute(attribute),
			"time", 1f,
			"onupdate", "UpdateColoringAnimation",
			"onupdatetarget", gameObject
			));
		currentAttribute = attribute;
	}
	
	Vector3 GetColorVectorByAttribute(int attribute) {
		switch (attribute) {
		case 0: // 炎
			return new Vector3 (1, 0, 0);
		case 1: // 水
			return new Vector3 (0, 0, 1);
		case 2: // 風
			return new Vector3 (0, 1, 0);
		default:
			return new Vector3 (1, 1, 1);
		}
	}
	
	void UpdateColoringAnimation(Vector3 value) {
		spriteRenderer.color = new Color(
			0.7f + 0.3f * value.x,
			0.7f + 0.3f * value.y,
			0.7f + 0.3f * value.z
		);
		Color color = new Color (value.x, value.y, value.z);
		color.a = 0.15f;
		particleSystem.startColor = color;
	}
	
	void EnemyScaleAnimation() {
		float time = 0.3f;
		Hashtable parameters = new Hashtable();
		parameters.Add ("x", 1.88f);
		parameters.Add ("y", 1.8f);
		parameters.Add ("time", time);
		parameters.Add ("easetype", iTween.EaseType.easeInOutCubic);
		// parameters.Add ("oncomplete","EnemyScaleReduction");
		// parameters.Add ("oncompletetarget", gameObject);
		
		Hashtable parameters2 = new Hashtable();
		parameters2.Add ("x", 1.8f);
		parameters2.Add ("y", 1.93f);
		parameters2.Add ("time", time);
		parameters2.Add ("easetype", iTween.EaseType.easeInOutCubic);
		parameters2.Add ("oncomplete","EnemyScaleAnimation");
		parameters2.Add ("oncompletetarget", gameObject);
		
		iTweenExtention.SerialPlay (
			gameObject,
			(iTweenAction)iTween.ScaleTo, parameters,
			(iTweenAction)iTween.ScaleTo, parameters2
			);
	}
	
	void DispDieEffect(){
		Instantiate(enemyAttackEffect,transform.position,transform.rotation);
	}
	
	void DispDamagedEffect(){
		Instantiate(hitEffect,transform.position,transform.rotation);
	}
	
	void Move() {
		iTween.MoveTo (gameObject, iTween.Hash(
			"x", 0,
			"y", 4f,
			"z", 0,
			"time", 1.5f,
			"easeType", iTween.EaseType.easeInBack
			));
	}
	
	public void SetPower(int x) {
		enemyPower = x;
	}
	
	public int GetPower() {
		return enemyPower;
	}
	
	float CalcDamageRate(int enemyAttr, int bulletAttr){
		if (enemyAttr == bulletAttr)
			return 1;
		
		if ((enemyAttr + 1) % 3 == bulletAttr)
			return 2;
		
		return 0.5f;
	}
	
	private void SetDirection() {
		Move ();
	}
	
	public void OnTriggerEnter2D (Collider2D bulletCollider) {
		string layerName = LayerMask.LayerToName (bulletCollider.gameObject.layer);
		
		if (layerName == "Bullet") {
			if (die) return;
			
			// プレイヤーのlocalScaleでどちら動くかを判定
			if (transform.localScale.y >= 0) {
				transform.Translate(Vector2.up * -1);
			}
			
			Bullet bullet = bulletCollider.GetComponent<Bullet>();
			float damage =  bullet.power * CalcDamageRate(
				currentAttribute,
				bullet.GetAttribute()
				);
			
			HP -= damage;
			hpGauge.Increase(-damage / maxHP);
			
			Debug.Log ("HP: " + HP.ToString());
			KnockBack(bullet.GetAttribute(), bullet.gameObject.rigidbody2D.velocity);
			
			Destroy (bulletCollider.gameObject);
			gameController.AttackSuccess();
			
			if (HP <= 0) {
				die = true;
				DispDieEffect();
				Destroy(gameObject);
				gameController.GameEnd();
			} else {
				DispDamagedEffect();
			}
		}
	}
	
	void KnockBack(int bulletAttribute, Vector3 bulletDir) {
		bulletDir = Vector3.Normalize (bulletDir);
		float knockBackRate = 0.6f;
		
		float power = knockBackRate * 120 * Time.deltaTime;
		iTween.MoveBy (gameObject, iTween.Hash(
			"x", power * bulletDir.x,
			"y", power * bulletDir.y,
			"time", 0.4f,
			"oncomplete", "SetDirection",
			"oncompletetarget", gameObject
			));
	}
}
