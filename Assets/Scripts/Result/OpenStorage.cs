﻿using UnityEngine;
using System.Collections;

public class OpenStorage : MonoBehaviour {

	private float timer;
	private float interval = 1.0f;
	private int nextStorage;

	// Use this for initialization
	void Start () {
		timer = 0;
		nextStorage = 1;
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if(timer > interval){
			ChangeSprite();
			nextStorage += 1;
			timer = 0;
			if(nextStorage == 12){
				Application.LoadLevel("Main");
			}
		}
	}

	void ChangeSprite(){
		switch(nextStorage){
		case 1:
			GameObject.Find("StorageBox1").SendMessage("Changetex");
			return;
		case 2:
			GameObject.Find("StorageBox2").SendMessage("Changetex");
			return;
		case 3:
			GameObject.Find("StorageBox3").SendMessage("Changetex");
			return;
		case 4:
			GameObject.Find("StorageBox4").SendMessage("Changetex");
			return;
		case 5:
			GameObject.Find("StorageBox5").SendMessage("Changetex");
			return;
		case 6:
			GameObject.Find("StorageBox6").SendMessage("Changetex");
			return;
		default:
			return;
		}
	}

	void OnMouseDown(){
		timer += interval;
	}
}
