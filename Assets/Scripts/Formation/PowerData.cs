﻿using UnityEngine;
using System.Collections;

public class PowerData : MonoBehaviour {

	//attribute 0: 火, 1: 水, 2: 風

	private int no1_Power = 10;
	private int hitPoint1 = 100;
	private int attribute1= 0;
	private int bullet1 = 10;

	private int no2_Power = 20;
	private int hitPoint2 = 200;
	private int attribute2 = 1;
	private int bullet2 = 8;

	private int no3_Power = 30;
	private int hitPoint3 = 300;
	private int attribute3 = 2;
	private int bullet3 = 20;

	private int no4_Power = 40;
	private int hitPoint4 = 400;
	private int attribute4 = 0;
	private int bullet4 = 8;

	private int no5_Power = 50;
	private int hitPoint5 = 500;
	private int attribute5 = 1;
	private int bullet5 = 5;

	private int no6_Power = 60;
	private int hitPoint6 = 600;
	private int attribute6 = 2;
	private int bullet6 = 9;

	public int AttackData(int number){
		switch(number){
		case 1:
			return no1_Power;
		case 2:
			return no2_Power;
		case 3:
			return no3_Power;
		case 4:
			return no4_Power;
		case 5:
			return no5_Power;
		case 6:
			return no6_Power;
		default:
			return no1_Power;
		}
	}
	public int HpData(int number){
		switch(number){
		case 1:
			return hitPoint1;
		case 2:
			return hitPoint2;
		case 3:
			return hitPoint3;
		case 4:
			return hitPoint4;
		case 5:
			return hitPoint5;
		case 6:
			return hitPoint6;
		default:
			return hitPoint1;
		}
	}

	public int Atribute(int number){
		switch(number){
		case 1:
			return attribute1;
		case 2:
			return attribute2;
		case 3:
			return attribute3;
		case 4:
			return attribute4;
		case 5:
			return attribute5;
		case 6:
			return attribute6;
		default:
			return attribute1;
		}
	}

	public int BulletRemaining(int number){
		switch(number){
		case 1:
			return bullet1;
		case 2:
			return bullet2;
		case 3:
			return bullet3;
		case 4:
			return bullet4;
		case 5:
			return bullet5;
		case 6:
			return bullet6;
		default:
			return bullet1;
		}
	}

}
