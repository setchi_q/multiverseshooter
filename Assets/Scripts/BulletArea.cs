﻿using UnityEngine;
using System.Collections;

public class BulletArea : MonoBehaviour {

	void OnTriggerExit2D (Collider2D c)
	{
		Vector3 rebound2 = c.gameObject.rigidbody2D.velocity;
		rebound2.x = -rebound2.x;

		c.rigidbody2D.velocity = rebound2;
	}
}
