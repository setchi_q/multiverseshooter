﻿using UnityEngine;
using System.Collections;

public class Village : MonoBehaviour {
	private StageTransition stageTransition;

	public GameObject Friend_menu;
	public GameObject stage;
	public int stageNo;

	private Friend friend;

	private GameObject target;
	private TouchAudio touch;
	
	void Start() {
		target = GameObject.Find("touchAudio");
		if(target){
			touch = target.GetComponent<TouchAudio>();
		}
		stageTransition = gameObject.GetComponent<StageTransition>();
	}

	void OnMouseDown() {
		if(target){
			touch.touch();
		}
		Friend_menu.SetActive(true);
		friend = Friend_menu.GetComponent<Friend>();
		friend.ChangeStage(stageNo);
		friend.setAct();
		stage.SetActive(false);
	}

	public void Stage(){
		iTween.ScaleTo (gameObject, iTween.Hash("x", 1.4f, "y", 1.4f, "time", 0.5f, "easetype", iTween.EaseType.easeOutBounce));
		stageTransition.TransitionAfterAnimation ("CoreAction", gameObject.transform.position);
	}
}
