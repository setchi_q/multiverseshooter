﻿using UnityEngine;
using System.Collections;

public class ChangeSprite : MonoBehaviour {
	SpriteRenderer MainSpriteRenaderer;
	
	private int monsterNo = 1;
	private GameObject party;
	private DataOutPut dataOutput;
	
	public Sprite no1;
	public Sprite no2;
	public Sprite no3;
	public Sprite no4;
	public Sprite no5;
	public Sprite no6;

	public int position;//over = 0 center = 1 under = 2
	
	void Start(){
		
		MainSpriteRenaderer = gameObject.GetComponent<SpriteRenderer>();
		party = GameObject.Find("Party");
		if(party){
			dataOutput = party.GetComponent<DataOutPut>();
			switch(position){
			case 0:
				monsterNo = dataOutput.OutputLeft();
				break;
			case 1:
				monsterNo = dataOutput.OutputCenter();
				break;
			case 2:
				monsterNo = dataOutput.OutputRight();
				break;
			default:
				monsterNo = 1;
				break;
			}
			Change();
		}
	}
	
	//public void ChangeNO(int number){
	//	monsterNo = number;
	//	Change();
	//}
	
	void Change(){
		switch(monsterNo){
		case 1:
			MainSpriteRenaderer.sprite = no1;
			return;
		case 2:
			MainSpriteRenaderer.sprite = no2;
			return;
		case 3:
			MainSpriteRenaderer.sprite = no3;
			return;
		case 4:
			MainSpriteRenaderer.sprite = no4;
			return;
		case 5:
			MainSpriteRenaderer.sprite = no5;;
			return;
		case 6:
			MainSpriteRenaderer.sprite = no6;
			return;
		default:
			return;
		}
	}
}
