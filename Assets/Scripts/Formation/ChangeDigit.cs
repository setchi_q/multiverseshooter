﻿using UnityEngine;
using System.Collections;

public class ChangeDigit : MonoBehaviour {

	private int left;
	private int center;
	private int right;
	private int totalHP;
	private PowerData data;
	private int[] digit = new int[4];
	private TheDigit[] x = new TheDigit[4]; 
	private bool flag = false;

	public GameObject digit1;
	public GameObject digit10;
	public GameObject digit100;
	public GameObject digit1000;

	void Start () {
		left = 0;
		center = 1;
		right = 2;
		data = GetComponent<PowerData>();
		x[0] = digit1.GetComponent<TheDigit>();
		x[1] = digit10.GetComponent<TheDigit>();
		x[2] = digit100.GetComponent<TheDigit>();
		x[3] = digit1000.GetComponent<TheDigit>();
	}

	public void ChangeLeft(int number){
		left = number;
		TotalHP();
	}

	public void ChangeCenter(int number){
		center = number;
		TotalHP();
	}

	public void ChangeRight(int number){
		right = number;
		TotalHP();
	}

	void TotalHP(){
		totalHP = 0;
		totalHP += data.HpData(left);
		totalHP += data.HpData(center);
		totalHP += data.HpData(right);
		if(totalHP>=1000){
			flag = true;
		}else{
			flag = false;
		}
		Cast(totalHP);
		Output();
	}

	void Cast(int totalHP){
		digit[0] = totalHP % 10;
		totalHP = (totalHP-digit[0]) / 10;
		digit[1] = totalHP % 10;
		totalHP = (totalHP-digit[1]) / 10;
		digit[2] = totalHP % 10;
		totalHP = (totalHP-digit[2]) / 10;
		if(flag){
			digit[3] = totalHP % 10;
		}
	}

	void Output(){
		x[0].ChangeSprite(digit[0]);
		x[1].ChangeSprite(digit[1]);
		x[2].ChangeSprite(digit[2]);
		if(flag){
			x[3].ChangeSprite(digit[3]);
		}else{
			x[3].ChangeSprite(10);
		}
	}
}
