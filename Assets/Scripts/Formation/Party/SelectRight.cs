﻿using UnityEngine;
using System.Collections;

public class SelectRight : MonoBehaviour {
	
	private int monsterNo =3;
	
	public GameObject left;
	public GameObject center;
	public GameObject right;
	
	public GameObject No1;
	public GameObject No2;
	public GameObject No3;
	public GameObject No4;
	public GameObject No5;
	public GameObject No6;
	
	private bool isTouch = false;
	private Vector3 selfPoint;
	private Vector3 startPos;
	private Vector3 endPos;
	
	private GameObject circle1;

	private float x1 = -1.6f;
	private float x2 = -0.275f;
	private float x3 = 1.6f;
	private float y1 = 5.5f;
	private float y2 = 4.0f;
	
	private Vector3 screenPoint;
	private Vector3 offset;
	private Vector3 currentPosition;

	private GameObject touchAudio;
	private TouchAudio touch;
	
	// Use this for initialization
	void Start () {
		touchAudio = GameObject.Find("touchAudio2");
		if(touchAudio){
			touch = touchAudio.GetComponent<TouchAudio>();
		}
	}
	
	public void ChangeMonster(int number){
		monsterNo = number;
	}
	
	
	void OnMouseDown()
	{
		isTouch = true;
		//カメラから見たオブジェクトの現在位置を画面位置座標に変換
		screenPoint = Camera.main.WorldToScreenPoint(transform.position);
		
		//取得したscreenPointの値を変数に格納
		float x = Input.mousePosition.x;
		float y = Input.mousePosition.y;
		
		//オブジェクトの座標からマウス位置(つまりクリックした位置)を引いている。
		//これでオブジェクトの位置とマウスクリックの位置の差が取得できる。
		//ドラッグで移動したときのずれを補正するための計算だと考えれば分かりやすい
		offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(x, y, screenPoint.z));
	}
	
	void OnMouseDrag()
	{
		//ドラッグ時のマウス位置を変数に格納
		float x = Input.mousePosition.x;
		float y = Input.mousePosition.y;
		
		//ドラッグ時のマウス位置をシーン上の3D空間の座標に変換する
		Vector3 currentScreenPoint = new Vector3(x, y, screenPoint.z);
		
		//クリックした場所の差から、オブジェクトを移動する座標位置を求める
		currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint) + offset;
		
		//オブジェクトの位置を変更する
		//transform.position = currentPosition;
		if(circle1){
			circle1.transform.position = currentPosition;
		}
	}
	void Update () {
		if (isTouch && Input.GetMouseButtonDown (0)) {
			startPos = Input.mousePosition;
			selfPoint = Camera.main.WorldToScreenPoint(startPos);
			switch(monsterNo){
			case 1:
				circle1 = Instantiate(No1,transform.position,transform.rotation) as GameObject;
				break;
			case 2:
				circle1 = Instantiate(No2,transform.position,transform.rotation) as GameObject;
				break;
			case 3:
				circle1 = Instantiate(No3,transform.position,transform.rotation) as GameObject;
				break;
			case 4:
				circle1 = Instantiate(No4,transform.position,transform.rotation) as GameObject;
				break;
			case 5:
				circle1 = Instantiate(No5,transform.position,transform.rotation) as GameObject;
				break;
			case 6:
				circle1 = Instantiate(No6,transform.position,transform.rotation) as GameObject;
				break;
			default:
				circle1 = Instantiate(No3,transform.position,transform.rotation) as GameObject;
				break;
			}
		}else if (isTouch && Input.GetMouseButtonUp (0)) {
			if(touchAudio){
				touch.touch();
			}
			Destroy(circle1);
			endPos = Input.mousePosition;
			Vector3 mouseUpWorldPoint = Camera.main.ScreenToWorldPoint(new Vector3(
				endPos.x,
				endPos.y,
				selfPoint.z
				));
			if((x1 <= mouseUpWorldPoint.x)&&(x2 > mouseUpWorldPoint.x)){
				if((y2 <= mouseUpWorldPoint.y)&&(y1 >= mouseUpWorldPoint.y)){
					left.SendMessage ("ChangeNo", monsterNo);
				}
			}else if((x2 <= mouseUpWorldPoint.x)&&(x3 > mouseUpWorldPoint.x)){
				if((y2 <= mouseUpWorldPoint.y)&&(y1 > mouseUpWorldPoint.y)){
					center.SendMessage ("ChangeNo", monsterNo);
				}
			}
			isTouch = false;
		}
	}
}
