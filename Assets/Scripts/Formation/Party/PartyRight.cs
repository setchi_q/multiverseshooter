using UnityEngine;
using System.Collections;

public class PartyRight : MonoBehaviour {
	
	SpriteRenderer MainSpriteRenaderer;
	
	//public GameObject parent;
	
	//Monster.No
	public Sprite no1;
	public Sprite no2;
	public Sprite no3;
	public Sprite no4;
	public Sprite no5;
	public Sprite no6;

	private int setNo;
	private int power;
	private int hitPoint;
	
	private int falseAttribute;
	private int attribute;
	private int attribute1;
	private int attribute2;
	
	private PartyLeft left_att;
	private PartyCenter center_att;
	private SelectRight no;
	
	private DataOutPut data;
	private PowerData data2;
	public GameObject left;
	public GameObject center;
	
	// Use this for initialization
	void Start () {
		setNo = 3;
		attribute = 2;

		MainSpriteRenaderer = gameObject.GetComponent<SpriteRenderer>();
		MainSpriteRenaderer.sprite = no3;

		data = GetComponentInParent<DataOutPut>();
		data2 = GetComponent<PowerData>();

		left_att = left.GetComponent<PartyLeft>();

		center_att = center.GetComponent<PartyCenter>();

		no =GetComponent<SelectRight>(); 
	}
	
	public int Att(){
		return attribute;
	}
	
	public void PersonalData(int number,bool set){

		falseAttribute = data2.Atribute(number);
		attribute1 =  left_att.Att();
		attribute2 = center_att.Att();
		if(!set){
			if(falseAttribute == attribute){
				power = data2.AttackData(number);
				hitPoint = data2.HpData(number);
			}else{
				if(falseAttribute == attribute1){
					left_att.ChangeParty(setNo);
					attribute = data2.Atribute(number);
					power = data2.AttackData(number);
					hitPoint = data2.HpData(number);
				}else if(falseAttribute == attribute2){
					center_att.ChangeParty(setNo);
					attribute = data2.Atribute(number);
					power = data2.AttackData(number);
					hitPoint = data2.HpData(number);
				}
			}
		}else{
			attribute = data2.Atribute(number);
			power = data2.AttackData(number);
			hitPoint = data2.HpData(number);
		}
	}
	
	public void ChangeNo(int number){
		if(setNo==number){
			return;
		}
		Debug.Log(setNo+"right setNo    "+number+"input  "+attribute+"att");
		PersonalData(number,false);
		Debug.Log (attribute+":att after right");
		setNo = number;
		data.ChangeRight(setNo);
		no.ChangeMonster(number);
		switch(number){
		case 1:
			MainSpriteRenaderer.sprite = no1;
			return;
		case 2:
			MainSpriteRenaderer.sprite = no2;
			return;
		case 3:
			MainSpriteRenaderer.sprite = no3;
			return;
		case 4:
			MainSpriteRenaderer.sprite = no4;
			return;
		case 5:
			MainSpriteRenaderer.sprite = no5;;
			return;
		case 6:
			MainSpriteRenaderer.sprite = no6;
			return;
		default:
			return;
		}
	}
	public void ChangeParty(int number){
		Debug.Log(setNo+"right setNo    "+number+"input  "+attribute+"att");
		PersonalData(number,true);
		Debug.Log (attribute+":att after right");
		setNo = number;
		data.ChangeRight(setNo);
		no.ChangeMonster(number);
		switch(number){
		case 1:
			MainSpriteRenaderer.sprite = no1;
			return;
		case 2:
			MainSpriteRenaderer.sprite = no2;
			return;
		case 3:
			MainSpriteRenaderer.sprite = no3;
			return;
		case 4:
			MainSpriteRenaderer.sprite = no4;
			return;
		case 5:
			MainSpriteRenaderer.sprite = no5;;
			return;
		case 6:
			MainSpriteRenaderer.sprite = no6;
			return;
		default:
			return;
		}
	}
}
