using UnityEngine;
using System.Collections;

public class DataOutPut : MonoBehaviour {

	private int leftMonster = 1;
	private int centerMonster = 2;
	private int rightMonster = 3;

	private int friendMonster = 1;

	private GameObject digit;
	private ChangeDigit changeDigit;

	private GameObject bullet;
	private ChangeBullet changeBullet;

	private int count;

	void Start(){
		count = 0;
		digit = GameObject.Find("TotalHp");
		if(digit != null){
			changeDigit = digit.GetComponent<ChangeDigit>();
			//ChangeLeft(1);
			//ChangeCenter(2);
			//ChangeRight(3);
		}
		bullet = GameObject.Find("Bulletrest");
		if(bullet){
			changeBullet = bullet.GetComponent<ChangeBullet>();
			//
			//
			//
		}
	}

	void Update(){
		if(count == 0 && Application.loadedLevelName!="Formation"){
			count = 1;
		}
		if(count == 1 && Application.loadedLevelName=="Formation"){
			digit = GameObject.Find("TotalHp");
			if(digit != null){
				changeDigit = digit.GetComponent<ChangeDigit>();
			}
			bullet = GameObject.Find("Bulletrest");
			if(bullet){
				changeBullet = bullet.GetComponent<ChangeBullet>();
			}
			count = 0;
		}
	}


	public void ChangeLeft(int number){
		leftMonster = number;
		if(digit){
			changeDigit.ChangeLeft(leftMonster);
		}
		if(bullet){
			changeBullet.ChangeLeft(leftMonster);
		}
	}
	public void ChangeCenter(int number){
		centerMonster = number;
		if(digit){
			changeDigit.ChangeCenter(centerMonster);
		}
		if(bullet){
			changeBullet.ChangeCenter(centerMonster);
		}
	}
	public void ChangeRight(int number){
		rightMonster = number;
		if(digit){
			changeDigit.ChangeRight(rightMonster);
		}
		if(bullet){
			changeBullet.ChangeRight(rightMonster);
		}
	}

	public void ChangeFriend(int number){
		friendMonster = number;
		Debug.Log(friendMonster+":friendMonster");
	}

	public int OutputLeft(){
		return leftMonster;
	}
	public int OutputCenter(){
		return centerMonster;
	}
	public int OutputRight(){
		return rightMonster;
	}
	public int OutputFriend(){
		return friendMonster;
	}
}
