﻿using UnityEngine;
using System.Collections;

public class CircularGauge : MonoBehaviour {
	public GameObject valueObj;
	private int value;

	// Use this for initialization
	void Start () {
		value = 0;

		for (int i = 0; i < 360; i += 3) {
			GameObject obj = (GameObject)Instantiate(valueObj, Vector3.zero, Quaternion.identity);
			obj.transform.parent = gameObject.transform;
			obj.transform.localScale = new Vector3 (7.75f, 2.5f, 2.5f);
			obj.transform.position = obj.transform.parent.position;
			
			obj.transform.localEulerAngles = new Vector3(0, 0, (float)i);
			obj.GetComponent<OneDegreeController> ().SetId(i);
		}
	}
	
	// Update is called once per frame
	void Update () {
	}

	public int GetValue() {
		return value;
	}

	public void SetValue(float value) {
		if (value < 0) {
			value = 0;

		} else if (value > 1) {
			value = 1;
		}

		this.value = Mathf.RoundToInt(360 * value);
	}
}
