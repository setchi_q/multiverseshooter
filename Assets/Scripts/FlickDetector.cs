﻿using UnityEngine;
using System.Collections;

public class FlickDetector : MonoBehaviour {
	private GameController gameController;
	private float buttonRegionHeight = -2.5f;
	private Vector3 startPos;
	private Vector3 endPos;
	private Vector3 selfPoint;

	private enum FlickState {
		Standby,
		Move,
		Up
	};
	private FlickState flickState;

	void Start () {
		flickState = FlickState.Standby;
		gameController = GameObject.Find ("GameController").GetComponent<GameController>();
	}
	
	void Update () {
		switch (flickState) {
		case FlickState.Standby:
			if (Input.GetMouseButtonDown(0)) {
				startPos = Input.mousePosition;
				selfPoint = Camera.main.WorldToScreenPoint(transform.position);
				flickState = FlickState.Move;
			}
			break;

		case FlickState.Move:
			if (Input.GetMouseButtonUp (0)) {
				flickState = FlickState.Up;
			}
			break;

		case FlickState.Up:
			flickState = FlickState.Standby;

			endPos = Input.mousePosition;
			Vector3 mouseUpWorldPoint = Camera.main.ScreenToWorldPoint(new Vector3(
				endPos.x,
				endPos.y,
				selfPoint.z
				));
			
			// ボタンの領域内だったら無視
			if (isButtonRegion(mouseUpWorldPoint.y)) {
				return;
			}
			
			// 2点間の距離が小さかったら（タップだったら）無視する。範囲は要調整
			if (Vector3.Distance(endPos, startPos) < 5)
				return;
			
			// 弾の方向
			Vector3 bulletDir = Vector3.Normalize (endPos - startPos);
			
			// 下方向のスワイプなら無視
			if (bulletDir.y < 0) {
				return;
			}
			
			gameController.Shoot (bulletDir, new Vector3(
				Input.mousePosition.x,
				Input.mousePosition.y,
				selfPoint.z
				));
			break;
		};
	}
	
	// とりあえずy座標だけ見て、ボタン領域を判断してる
	bool isButtonRegion(float y) {
		return y < buttonRegionHeight;
	}
}
