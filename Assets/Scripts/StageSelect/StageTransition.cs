﻿using UnityEngine;
using System.Collections;

public class StageTransition : MonoBehaviour {
	public GameObject mask;
	private SpriteRenderer maskSpriteRenderer;

	private float animationTime = 1.2f;

	void Start () {
		maskSpriteRenderer = mask.GetComponent<SpriteRenderer> ();
	}

	public void TransitionAfterAnimation(string sceneName, Vector3 pos) {
		mask.SetActive (true);

		Hashtable iTweenCameraParams = new Hashtable();
		iTweenCameraParams.Add ("x", pos.x);
		iTweenCameraParams.Add ("y", pos.y);
		iTweenCameraParams.Add ("z", pos.z);
		iTweenCameraParams.Add ("time", animationTime);
		iTweenCameraParams.Add ("oncomplete", "Transition");
		iTweenCameraParams.Add ("oncompleteparams", sceneName);
		iTweenCameraParams.Add ("oncompletetarget", gameObject);
		iTweenCameraParams.Add ("easetype", iTween.EaseType.easeInCubic);
		iTween.MoveTo (Camera.main.gameObject, iTweenCameraParams);

		StartFadeOut ();
	}

	void StartFadeOut() {
		Hashtable iTweenMaskParams = new Hashtable ();
		iTweenMaskParams.Add ("from", new Vector2(0, 0));
		iTweenMaskParams.Add ("to", new Vector2(1, 0));
		iTweenMaskParams.Add ("time", animationTime);
		iTweenMaskParams.Add ("easetype", iTween.EaseType.easeInQuint);
		iTweenMaskParams.Add ("onupdate", "ChangeMaskOpacity");
		iTweenMaskParams.Add ("onupdatetarget", gameObject);
		
		iTween.ValueTo (gameObject, iTweenMaskParams);
	}


	void Transition(string sceneName) {
		Application.LoadLevel(sceneName);
	}

	void ChangeMaskOpacity(Vector2 value) {
		Color color = maskSpriteRenderer.color;
		color.a = value.x;
		maskSpriteRenderer.color = color;
	}
}
