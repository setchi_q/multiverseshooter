﻿using UnityEngine;
using System.Collections;

public class EnemyEffect : MonoBehaviour {

	void Start(){

		ParticleSystem particleSystem = GetComponent <ParticleSystem>();
		Destroy(this.gameObject, particleSystem.duration);
	
	}
}
