using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	public GameObject menu;
	private int count = 0;

	void OnMouseDown(){
		if(count == 0){
			//iTween.ShakePosition(menu, iTween.Hash("x", 0.05));
			iTween.MoveTo(menu, iTween.Hash("x",-2.7, "islocal", true));
			count += 1;
		}
	}
	void OnMouseEnter(){
		if(count == 0){
			//iTween.ShakePosition(menu, iTween.Hash("x", 0.05));
			iTween.MoveTo(menu, iTween.Hash("x",-2.7, "islocal", true));
			count += 1;
		}
	}
	void OnMouseExit(){
		if(count == 1){
			iTween.MoveTo(menu, iTween.Hash("x", -3, "islocal", true));
			count -= 1;
		}
	}
	void OnMouseUp(){
		if(count == 1){
			iTween.MoveTo(menu, iTween.Hash("x", -3, "islocal", true));
			count -= 1;
		}
	}
}
