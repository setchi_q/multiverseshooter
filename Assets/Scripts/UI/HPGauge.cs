﻿using UnityEngine;
using System.Collections;

public class HPGauge : MonoBehaviour {
	public float maxOffsetX;
	public float maxScaleX;
	public GameObject valueObject;
	public bool animate;

	private float currentValue;
	// Use this for initialization
	void Start () {
		SetValue (1.0f);
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void SetValue(float percent) {
		currentValue = percent;

		if (animate) {
			iTween.MoveTo (valueObject, iTween.Hash ("x", maxOffsetX * (1f - currentValue), "time", 0.6f, "islocal", true, "easetype", iTween.EaseType.easeOutCubic));
			iTween.ScaleTo (valueObject, iTween.Hash ("x", maxScaleX * currentValue, "time", 0.6f, "easetype", iTween.EaseType.easeOutCubic));
		} else {
			valueObject.transform.localPosition = new Vector3(maxOffsetX * (1f - currentValue), 0, 0);
			valueObject.transform.localScale = new Vector3(maxScaleX * currentValue, 1, 1);
		}
	}

	public float GetValue() {
		return currentValue;
	}

	public void Increase(float increaseValue) {
		SetValue (GetValue() + increaseValue);
	}
}
