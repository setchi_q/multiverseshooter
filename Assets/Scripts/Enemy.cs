using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public float speed;
	public GameObject destroy;
	public GameObject hitEffect;
	public GameObject enemyAttackEffect;
	public float HP = 2;//HitPoint
	public int attribute;//not touch
	public int enemyPower;//not touch
	
	public Vector2 speed2 = new Vector2(0.05f, 0.05f);
	// ターゲットとなるオブジェクト
	public GameObject targetObject1;
	public GameObject targetObject2;
	public GameObject targetObject3;

	public GameObject playerHPGaugeObject;
	
	// 現在位置を代入する為の変数
	private Vector2 Position;
	// Use this for initialization
	private int swc;//switch
	
	private HPGauge playerHpGauge;
	
	private GameController gameController;
	
	void Start () {
		swc = Random.Range(0,3);
		SetDirection ();
		playerHpGauge = GameObject.Find ("PlayerHPGauge").GetComponent<HPGauge> ();
		gameController = GameObject.Find ("GameController").GetComponent<GameController>();
		EnemyScaleAnimation ();
	}
	
	// Update is called once per frame
	void Update () {
		if(swc == 0){
			return;
		}else if(swc!=0){
			return;
		}
	}
	
	void EnemyScaleAnimation() {
		float time = 0.3f;
		Hashtable parameters = new Hashtable();
		parameters.Add ("x", 1.08f);
		parameters.Add ("y", 1f);
		parameters.Add ("time", time);
		parameters.Add ("easetype", iTween.EaseType.easeInOutCubic);
		// parameters.Add ("oncomplete","EnemyScaleReduction");
		// parameters.Add ("oncompletetarget", gameObject);
		
		Hashtable parameters2 = new Hashtable();
		parameters2.Add ("x", 1f);
		parameters2.Add ("y", 1.13f);
		parameters2.Add ("time", time);
		parameters2.Add ("easetype", iTween.EaseType.easeInOutCubic);
		parameters2.Add ("oncomplete","EnemyScaleAnimation");
		parameters2.Add ("oncompletetarget", gameObject);
		
		iTweenExtention.SerialPlay (
			gameObject,
			(iTweenAction)iTween.ScaleTo, parameters,
			(iTweenAction)iTween.ScaleTo, parameters2
			);
	}
	
	void DispDieEffect(){
		Instantiate(enemyAttackEffect,transform.position,transform.rotation);
	}
	
	void DispDamagedEffect(){
		Instantiate(hitEffect,transform.position,transform.rotation);
	}
	
	void DispAttackEffect(){
		Instantiate(destroy,transform.position,transform.rotation);
	}
	
	void Move(Vector2 direction){
		rigidbody2D.velocity = direction * speed;
	}
	
	public void SetPower(int x){
		enemyPower = x;
	}
	
	public int GetPower() {
		return enemyPower;
	}
	
	float CalcDamageRate(int enemyAttr, int bulletAttr){
		if (enemyAttr == bulletAttr)
						return 1.1f;
		
		if ((enemyAttr + 1) % 3 == bulletAttr)
			return 3f;
		
		return 0.7f;
	}
	
	private void SetDirection() {
		GameObject target = targetObject1;
		
		if (swc == 0){
			Move (transform.up * -1);
			return;
		}else if(swc == 1){
			target = targetObject1;
		}else if(swc == 2){
			target = targetObject2;
		}else if(swc == 3){
			target = targetObject3;
		}
		
		Vector3 targetPos = target.transform.localPosition;
		Vector2 velocity = (targetPos - transform.position).normalized;
		gameObject.rigidbody2D.velocity = velocity;
		
		Move (gameObject.rigidbody2D.velocity * 1);
	}
	
	public void OnTriggerEnter2D (Collider2D bulletCollider){
		string layerName = LayerMask.LayerToName (bulletCollider.gameObject.layer);
		
		if (layerName == "Bullet") {
			// プレイヤーのlocalScaleでどちら動くかを判定
			if (transform.localScale.y >= 0) {
				transform.Translate(Vector2.up * -1);
			}
			
			Bullet bullet = bulletCollider.GetComponent<Bullet>();
			HP -= bullet.power * CalcDamageRate(
				attribute,
				bullet.GetAttribute()
				);
			KnockBack(bullet.GetAttribute(), bullet.gameObject.rigidbody2D.velocity);
			
			Destroy (bulletCollider.gameObject);
			gameController.AttackSuccess();
			
			if (HP <= 0) {
				DispDieEffect();
				Destroy(gameObject);
			} else {
				DispDamagedEffect();
			}
		}
		
		if (layerName == "SaftyArea") {
			Destroy(gameObject);
			DispAttackEffect();
			
			// すっごい適当
			playerHpGauge.Increase(-0.2f);
			if (playerHpGauge.GetValue() < 0) {
				Application.LoadLevel("GameOver");
			}
		}
	}
	
	void KnockBack(int bulletAttribute, Vector3 bulletDir) {
		float knockBackRate = 1;
		if (attribute == bulletAttribute) {
				knockBackRate = 2;

		} else if ((attribute + 1) % 3 == bulletAttribute) {
				knockBackRate = 1;

		} else {
				knockBackRate = 1;
		}
		
		bulletDir = Vector3.Normalize (bulletDir);

		float power = knockBackRate * 120 * Time.deltaTime;
		iTween.MoveBy (gameObject, iTween.Hash(
			"x", power * bulletDir.x,
			"y", power * bulletDir.y,
			"time", 0.2f, "oncomplete",
			"SetDirection", "oncompletetarget", gameObject
			));
	}
}
