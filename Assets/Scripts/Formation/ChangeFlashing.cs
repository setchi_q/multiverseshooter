﻿using UnityEngine;
using System.Collections;

public class ChangeFlashing : MonoBehaviour {

	public GameObject set1;
	public GameObject set2;

	private ChangeFlashing setTo1;
	private ChangeFlashing setTo2;

	private bool flag;
	private float timer;
	private float interval = 0.3f;

	// Use this for initialization
	void Start () {
		flag = false;
		timer = 0;
		setTo1 = set1.GetComponent<ChangeFlashing>();
		setTo2 = set2.GetComponent<ChangeFlashing>();
	}

	void Update() {
		if(flag){
			timer += Time.deltaTime;
			if(timer > interval){
				renderer.enabled = !renderer.enabled;
				timer = 0;
			}
		}
	}

	public void Flashing(){
		flag = true;
		setTo1.Stop();
		setTo2.Stop();
	}

	public void Stop(){
		flag = false;
		renderer.enabled = true;
	}
}
